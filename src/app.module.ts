import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { CreateReferralModule } from './modules/signup-reward/signup-reward.module';
import { OrderRewardModule } from './modules/order-reward/order-reward.module';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'Referral',
      autoLoadModels: true,
      synchronize: true,
    }),
    // CustomerRewardsModule,
    // SalesOrdersModule,
    CreateReferralModule,
    OrderRewardModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
