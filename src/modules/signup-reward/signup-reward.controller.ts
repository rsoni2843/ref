import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseFilters,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { DataTypeValidationPipe } from 'src/pipes/data-type-validation.pipe';
import { SignupRewardService } from './signup-reward.service';
import { CreateReferralDto } from './dtos/create-referral.dto';
import { CustomHttpExceptionFilter } from 'src/filter';

@Controller('signup-rewards')
@UseFilters(CustomHttpExceptionFilter)
export class SignupRewardController {
  constructor(private readonly createReferralService: SignupRewardService) {}

  @UsePipes(new DataTypeValidationPipe(CreateReferralDto))
  @Post()
  async signupReferral(@Body() createReferralDto: CreateReferralDto) {
    return this.createReferralService.signupReferral(createReferralDto);
  }
}
