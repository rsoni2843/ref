import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { SalesOrder } from 'src/database/entity/sales-orders/sales-orders.entity';
import { CustomerRewards } from 'src/database/entity/customer-rewards/customer-rewards.entity';
import { SignupRewardService } from './signup-reward.service';
import { SignupRewardController } from './signup-reward.controller';
import { ReferralUser } from 'src/database/entity/referral-user/referral-user.entity';
import { ReferralCode } from 'src/database/entity/referral-codes/referral-code.entity';

@Module({
  imports: [
    SequelizeModule.forFeature([
      SalesOrder,
      CustomerRewards,
      ReferralUser,
      ReferralCode,
    ]),
  ],
  controllers: [SignupRewardController],
  providers: [SignupRewardService],
})
export class CreateReferralModule {}
