import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import {
  Actions,
  ERP_Status,
  OrderStatus,
} from 'src/database/entity/sales-orders/sales-orders.entity';

export class CreateReferralDto {
  @IsNotEmpty()
  @IsNumber()
  readonly customer_id: number;

  @IsNotEmpty()
  @IsString()
  readonly referral_code: string;
}
