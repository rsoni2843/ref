import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import {
  Actions,
  ERP_Status,
  OrderStatus,
  SalesOrder,
  Status,
} from 'src/database/entity/sales-orders/sales-orders.entity';
import { InjectModel } from '@nestjs/sequelize';
import { CustomerRewards } from 'src/database/entity/customer-rewards/customer-rewards.entity';
import { CreateReferralDto } from './dtos/create-referral.dto';
// import { ReferralUser } from 'src/database/entity/referral-user/referral-user.entity';
import * as crypto from 'crypto';
import { ReferralCode } from 'src/database/entity/referral-codes/referral-code.entity';
import { ReferralUser } from 'src/database/entity/referral-user/referral-user.entity';
function generateRandomString(length) {
  const charset =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-=[]{}|;:,.<>?';
  let result = '';
  let values = crypto.randomBytes(length);

  for (let i = 0; i < length; i++) {
    result += charset[values[i] % charset.length];
  }

  return result;
}
@Injectable()
export class SignupRewardService {
  constructor(
    @InjectModel(SalesOrder)
    private readonly salesOrder: typeof SalesOrder,
    @InjectModel(CustomerRewards)
    private readonly referralWallet: typeof CustomerRewards,
    @InjectModel(ReferralCode)
    private readonly createUser: typeof ReferralCode,
    @InjectModel(ReferralUser)
    private readonly referralUser: typeof ReferralUser,
  ) {}

  async signupReferral(createReferralDto: CreateReferralDto): Promise<any> {
    const { customer_id, referral_code } = createReferralDto;

    const checkReferralCode = await this.createUser.findOne({
      where: { referral_code },
    });
    if (!checkReferralCode) {
      throw new HttpException('Referral code not found', HttpStatus.NOT_FOUND);
    }
    if (!customer_id || !referral_code) {
      throw new HttpException(
        'Missing required parameter',
        HttpStatus.BAD_REQUEST,
      );
    }
    // Creating referral code for user
    const createReferralCode = await this.createUser.create({
      customer_id,
      referral_code: generateRandomString(6),
    });

    // Storing both referrer_id and referre_id in a table
    const createReferralUser = await this.referralUser.create({
      referee_id: customer_id,
      referrer_id: checkReferralCode.customer_id,
      code: referral_code,
    });

    // Creating wallet for that user
    const createWallet = await this.referralWallet.create({
      customer_id,
      earned_points: 50,
      reedemed_points: 0,
      available_points: 50,
      pending_points: 0,
    });

    // Creating signup action for that user
    const createSignupOrder = await this.salesOrder.create({
      customer_id,
      actions: Actions.SIGNUP_ACTION,
      order_id: '-',
      status: Status.COMPLETED,
      order_status: OrderStatus.NOT_APPLICABLE,
      erp_status: ERP_Status.NOT_APPLICABLE,
      earned_points: 50,
      available_points: 50,
      reedemed_points: 0,
    });

    const referrerIncrement = await this.referralWallet.findOne({
      where: { customer_id: checkReferralCode.customer_id },
    });
    referrerIncrement.earned_points = referrerIncrement.earned_points + 50;
    referrerIncrement.available_points =
      referrerIncrement.available_points + 50;
    await referrerIncrement.save();
    return {
      createReferralCode,
      createReferralUser,
      createWallet,
      createSignupOrder,
    };
  }
}
