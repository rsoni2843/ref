import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { DataTypeValidationPipe } from 'src/pipes/data-type-validation.pipe';
import { OrderRewardsDto } from './dtos/order-reward.dto';
import { OrderRewardService } from './order-reward.service';

@Controller('order-rewards')
export class OrderRewardController {
  constructor(private readonly orderRewardService: OrderRewardService) {}

  // @UsePipes(new DataTypeValidationPipe(OrderRewardsDto))
  @Post()
  async orderReward(@Body() orderRewardsDto: OrderRewardsDto) {
    return this.orderRewardService.orderReward(orderRewardsDto);
  }
}
