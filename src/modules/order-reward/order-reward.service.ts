import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import {
  Actions,
  ERP_Status,
  OrderStatus,
  SalesOrder,
  Status,
} from 'src/database/entity/sales-orders/sales-orders.entity';
import { InjectModel } from '@nestjs/sequelize';
import { CustomerRewards } from 'src/database/entity/customer-rewards/customer-rewards.entity';
// import { ReferralUser } from 'src/database/entity/referral-user/referral-user.entity';
import * as crypto from 'crypto';
import { ReferralCode } from 'src/database/entity/referral-codes/referral-code.entity';
import { ReferralUser } from 'src/database/entity/referral-user/referral-user.entity';
import { OrderRewardsDto } from './dtos/order-reward.dto';
function generateRandomString(length) {
  const charset =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-=[]{}|;:,.<>?';
  let result = '';
  let values = crypto.randomBytes(length);

  for (let i = 0; i < length; i++) {
    result += charset[values[i] % charset.length];
  }

  return result;
}
@Injectable()
export class OrderRewardService {
  constructor(
    @InjectModel(SalesOrder)
    private readonly salesOrder: typeof SalesOrder,
    @InjectModel(CustomerRewards)
    private readonly referralWallet: typeof CustomerRewards,
    @InjectModel(ReferralCode)
    private readonly createUser: typeof ReferralCode,
    @InjectModel(ReferralUser)
    private readonly referralUser: typeof ReferralUser,
  ) {} //

  async orderReward(orderRewardsDto: OrderRewardsDto): Promise<any> {
    const {
      order_status,
      status,
      order_id,
      customer_id,
      earned_points,
      spend_points,
      erp_status,
      actions,
    } = orderRewardsDto;

    if (!customer_id || !order_id || !order_status || !status) {
      throw new HttpException(
        'Missing required parameter',
        HttpStatus.BAD_REQUEST,
      );
    }

    const wallet = await this.referralWallet.findOne({
      where: { customer_id },
    });

    console.log({ wallet });

    let makeEarnOrder;
    let makeSpendOrder;
    if (actions === Actions.EARN_ACTION) {
      wallet.earned_points = wallet.earned_points + earned_points;
      wallet.pending_points = wallet.pending_points + earned_points;
      makeEarnOrder = await this.salesOrder.create({
        customer_id,
        earned_points,
        reedemed_points: 0,
        available_points: 0,
        order_status,
        actions,
        status,
        erp_status: null,
      });
      wallet.save();
      await makeEarnOrder.save();
    } else if (actions === Actions.SPEND_ACTION) {
      if (wallet.available_points < spend_points) {
        throw new HttpException(
          'Spend points cannot be greater than available points',
          HttpStatus.BAD_REQUEST,
        );
      }
      wallet.reedemed_points = spend_points;
      wallet.available_points = wallet.available_points - spend_points;
      makeSpendOrder = await this.salesOrder.create({
        customer_id,
        earned_points: 0,
        reedemed_points: spend_points,
        available_points: 0,
        order_status,
        actions,
        status,
        erp_status: null,
      });
      wallet.save();
      await makeSpendOrder.save();
    }
    return { wallet, makeEarnOrder, makeSpendOrder };
  }
}
