import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import {
  Actions,
  ERP_Status,
  OrderStatus,
  Status,
} from 'src/database/entity/sales-orders/sales-orders.entity';

export class OrderRewardsDto {
  @IsNotEmpty()
  @IsNumber()
  readonly customer_id: number;

  @IsNotEmpty()
  @IsNumber()
  readonly order_id: number;

  @IsEnum(OrderStatus)
  @IsNotEmpty()
  readonly order_status: string;

  @IsEnum(Status)
  @IsNotEmpty()
  readonly status: string;

  @IsEnum(Actions)
  @IsNotEmpty()
  readonly actions: string;

  // @IsNotEmpty()
  @IsNumber()
  readonly earned_points?: number;

  // @IsNotEmpty()
  @IsNumber()
  readonly spend_points?: number;

  @IsEnum(ERP_Status)
  @IsString()
  readonly erp_status: string | null;
}
