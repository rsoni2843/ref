import { OrderRewardService } from './order-reward.service';
import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { OrderRewardController } from './order-reward.controller';
import { SalesOrder } from 'src/database/entity/sales-orders/sales-orders.entity';
import { CustomerRewards } from 'src/database/entity/customer-rewards/customer-rewards.entity';
import { ReferralUser } from 'src/database/entity/referral-user/referral-user.entity';
import { ReferralCode } from 'src/database/entity/referral-codes/referral-code.entity';

@Module({
  imports: [
    SequelizeModule.forFeature([
      SalesOrder,
      CustomerRewards,
      ReferralUser,
      ReferralCode,
    ]),
  ],
  controllers: [OrderRewardController],
  providers: [OrderRewardService],
})
export class OrderRewardModule {}
