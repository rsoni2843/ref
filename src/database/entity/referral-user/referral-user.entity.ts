import { INTEGER, UUIDV4 } from 'sequelize';
import {
  Column,
  Model,
  Table,
  PrimaryKey,
  AutoIncrement,
} from 'sequelize-typescript';

@Table({
  tableName: 'referral_user',
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at',
})
export class ReferralUser extends Model {
  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @Column
  referee_id: string;

  @Column
  referrer_id: string;

  @Column
  code: string;
}
