import { INTEGER, UUIDV4 } from 'sequelize';
import {
  Column,
  Model,
  Table,
  PrimaryKey,
  NotNull,
  AutoIncrement,
  Default,
} from 'sequelize-typescript';

@Table({
  tableName: 'rewards_customerrewards',
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at',
})
export class CustomerRewards extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  // @Default(UUIDV4)
  id: number;

  @Column({ type: INTEGER.UNSIGNED, allowNull: false })
  customer_id: number;

  @Column({ type: INTEGER.UNSIGNED })
  earned_points: number;

  @Column({ type: INTEGER.UNSIGNED })
  reedemed_points: number;

  @Column({ type: INTEGER.UNSIGNED })
  available_points: number;

  @Column({ type: INTEGER.UNSIGNED })
  pending_points: number;
}
