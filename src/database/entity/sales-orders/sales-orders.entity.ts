import { DATEONLY, ENUM, INTEGER, NUMBER, TEXT, UUIDV4 } from 'sequelize';
import { Column, Model, Table, BeforeCreate } from 'sequelize-typescript';

export enum OrderStatus {
  AUTO_INVOICED = 'auto_invoiced',
  PAYMENT_RECEIVED = 'payment_received',
  PENDING = 'pending',
  NOT_APPLICABLE = '-',
}
export enum Status {
  PROCESSING = 'Processing',
  CANCELLED = 'Cancelled',
  COMPLETED = 'Completed',
  EXPIRED = 'Expired',
  APPLY = 'Apply',
  NOT_APPLICABLE = '-',
}

export enum ERP_Status {
  SHIPPED_COMPLETE = 'shipped_complete',
  NOT_APPLICABLE = '-',
}
export enum Actions {
  SIGNUP_ACTION = 'SIGNUP_ACTION',
  ADMIN_ACTION = 'ADMIN_ACTION',
  EARN_ACTION = 'EARN_ACTION',
  SPEND_ACTION = 'SPEND_ACTION',
  CANCEL_ACTION = 'CANCEL_ACTION',
  DEDUCT_ACTION = 'DEDUCT_ACTION',
  RETRIEVE_RETURN_ACTION = 'RETRIEVE_RETURN_ACTION',
  RETRIEVE_CANCEL_ACTION = 'RETRIEVE_CANCEL_ACTION',
  REVIEW_CREATION = 'REVIEW_CREATION',
}

@Table({
  tableName: 'rewards_salesorder',
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at',
})
export class SalesOrder extends Model {
  //   @PrimaryKey
  //   //   @Default(UUIDV4)
  //   //   @Column
  //   //   id: string;

  @Column({ type: INTEGER.UNSIGNED })
  order_id: number;

  @Column({ type: INTEGER.UNSIGNED, allowNull: false })
  customer_id: number;

  @Column({
    type: ENUM,
    values: Object.values(Status),
    defaultValue: null,
  })
  status: Status;

  @Column({
    type: ENUM,
    values: Object.values(OrderStatus),
    defaultValue: null,
  })
  order_status: OrderStatus;

  @Column({
    type: ENUM,
    values: Object.values(ERP_Status),
    defaultValue: null,
  })
  erp_status: ERP_Status;

  @Column({
    type: ENUM,
    values: Object.values(Actions),
    defaultValue: null,
  })
  actions: Actions;

  @Column({ type: INTEGER.UNSIGNED })
  earned_points: number;

  @Column({ type: INTEGER.UNSIGNED })
  available_points: number;

  @Column({ type: INTEGER.UNSIGNED })
  reedemed_points: number;

  @BeforeCreate
  static setExpiryDate(instance: SalesOrder) {
    // const createdDate = instance.getDataValue('created_at');
    // const expiryDate = new Date(createdDate);
    // expiryDate.setMonth(expiryDate.getMonth() + 6);
    // instance.setDataValue('expiry_date', expiryDate);
    const createdDate = instance.getDataValue('created_at');
    const expiryDate = new Date(createdDate);
    expiryDate.setMonth(expiryDate.getMonth() + 6);
    expiryDate.setHours(new Date().getHours());
    expiryDate.setMinutes(new Date().getMinutes());
    expiryDate.setSeconds(new Date().getSeconds());
    instance.setDataValue('expiry_date', expiryDate);
  }
  @Column({ type: DATEONLY })
  expiry_date: Date;
}
