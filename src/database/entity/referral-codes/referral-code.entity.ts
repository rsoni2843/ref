import { INTEGER, UUIDV4 } from 'sequelize';
import {
  Column,
  Model,
  Table,
  AutoIncrement,
  PrimaryKey,
} from 'sequelize-typescript';

@Table({
  tableName: 'referral_codes',
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at',
})
export class ReferralCode extends Model {
  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @Column
  referral_code: string;

  @Column
  customer_id: string;
}
