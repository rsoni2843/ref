import { Injectable, PipeTransform, BadRequestException } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class DataTypeValidationPipe implements PipeTransform {
  constructor(private readonly dto: any) {}

  async transform(value: any) {
    const object = plainToClass(this.dto, value);
    const errors = await validate(object);
    if (errors.length) {
      throw new BadRequestException('Invalid input data types');
    }
    return value;
  }
}
